package nl.avans.strategies;

import nl.avans.NodeStrategy;

import java.util.List;

public class PROBE extends NodeStrategy
{
    @Override
    public boolean perform(List<Boolean> inputs, String nodeName) {
        System.out.println("Probe '" + nodeName + "' = " + (inputs.get(0) ? 1 : 0));

        return inputs.get(0);
    }
}