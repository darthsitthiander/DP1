package nl.avans;

import java.util.List;

public class INPUT extends NodeStrategy
{
    private boolean inputValue;

    public void setInputValue (boolean inputValue)
    {
        this.inputValue = inputValue;
    }

    @Override
    public boolean perform(List<Boolean> inputs, String nodeName) {
        return this.inputValue;
    }
}
