package nl.avans;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Node extends Observable implements Observer {
    private NodeStrategy nodeStrategy;

    private boolean value;
    private String name;

    private List<Node> dependencies;
    private List<Boolean> inputs;

    public Node() {
        dependencies = new ArrayList<>();
        inputs = new ArrayList<>();
    }

    public void setNodeStrategy(NodeStrategy nodeStrategy) {
        this.nodeStrategy = nodeStrategy;
    }

    public boolean hasZeroDependencies()
    {
        return (this.dependencies.size() == 0);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addDependency(Node node) {
        dependencies.add(node);
        node.addObserver(this);
    }

    public void perform() {

        if (this.dependencies.size() == this.inputs.size()) {
            this.value = this.nodeStrategy.perform(this.inputs, this.name);

            setChanged();
            notifyObservers(this.value);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        boolean result              = (boolean) arg;

        this.inputs.add(result);

        this.perform();
    }
}
