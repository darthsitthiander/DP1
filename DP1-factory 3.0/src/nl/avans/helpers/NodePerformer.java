package nl.avans.helpers;

import nl.avans.Node;
import java.util.ArrayList;

public class NodePerformer
{
    public static void perform (ArrayList<Node> nodes)
    {
        System.out.println("Output for all probe nodes: \n");

        nodes.stream().filter(node -> node.hasZeroDependencies()).forEach(nl.avans.Node::perform);

        System.out.println("...bye");
    }
}
