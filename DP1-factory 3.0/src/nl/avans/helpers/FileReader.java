package nl.avans.helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class FileReader
{
    private ArrayList<String> readedLines;

    public FileReader (String fileNameToRead)
    {
        readedLines                                 = new ArrayList<>();

        try
        {
            java.io.FileReader fr                   = new java.io.FileReader(fileNameToRead);

            BufferedReader br                       = new BufferedReader(fr);
            String s;

            while ((s = br.readLine()) != null)
            {
                s = s.replaceAll("\\t", "").replaceAll("\\n", "").replaceAll(" ", "");

                this.readedLines.add(s);
            }

            fr.close();
        }
        catch (FileNotFoundException e)             { System.out.println("File: " + fileNameToRead + " does not exist. Exiting program."); System.exit(0); }
        catch (IOException e)                       { System.out.println("Failed to read file: " + fileNameToRead + ". Exiting program."); System.exit(0); }
    }

    public ArrayList<String> getReadedLines ()      { return this.readedLines; }
}
