package nl.avans;

import java.util.List;

public abstract class NodeStrategy implements Cloneable
{
    public abstract boolean perform (List<Boolean> inputs, String nodeName);

    public NodeStrategy clone ()
    {
        NodeStrategy clone                            = null;

        try
        {
            clone                                   = (NodeStrategy) super.clone();
        }
        catch (CloneNotSupportedException e)        { e.printStackTrace(); }

        return clone;
    }
}
