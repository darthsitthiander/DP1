package nl.avans;

import nl.avans.helpers.NodeBinder;
import nl.avans.helpers.NodePerformer;

public class Main
{
    public static void main (String[] args)
    {
        System.out.println("Logica simulator versie 0.14 build 1.");

        String fileName = "";//                                 = new InputReceiver().receiveInput();

        fileName                                        = (fileName.length() == 0) ? "circuit2.txt" : fileName;

        //circuit2.txt geeft geen uitkomst, omdat de node's verkeerd aan elkaar zijn gekoppeld en met ons oberser-observable worden de andere nodes nooit uitgevoerd
        //dit is optevangen door wanneer alle nodes gerunt zijn, te controleren of de ouputnodes een waarde hebben terug gegeven.

        NodeBinder binder               = new NodeBinder(fileName);

        NodePerformer.perform(binder.getNodes());
    }
}
