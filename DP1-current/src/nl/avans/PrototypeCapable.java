package nl.avans;

/**
 * Created by Sander on 30-4-2015.
 */
public interface PrototypeCapable extends Cloneable {
    public PrototypeCapable clone();
}