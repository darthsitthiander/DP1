package nl.avans;

/**
 * Created by Sander on 30-4-2015.
 */
public abstract class Observer {
    protected Subject s;

    public abstract boolean calculate();
}
