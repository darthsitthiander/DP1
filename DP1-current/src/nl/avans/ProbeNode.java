package nl.avans;

/**
 * Created by Sander on 30-4-2015.
 */
public class ProbeNode extends Node implements PrototypeCapable {
    public ProbeNode(String name) {
        super(name);
    }

    public ProbeNode() {
        super();
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public ProbeNode clone(){
        try {
            return (ProbeNode) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void perform() {
        System.out.println(this.name);
    }

    public String toString() {
        return "probe";
    }
}