package nl.avans;

import com.sun.corba.se.impl.resolver.INSURLOperationImpl;
import sun.plugin.perf.PluginRollup;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        /* PART I */
        ArrayList<Node> nodes = new ArrayList<Node>();

        InputNode iNode1 = (InputNode) PrototypeFactory.getInstance(PrototypeFactory.ModelType.INPUT);
        iNode1.setValue(true);
        iNode1.setName("A");
        InputNode iNode2 = (InputNode) PrototypeFactory.getInstance(PrototypeFactory.ModelType.INPUT);
        iNode2.setValue(false);
        iNode2.setName("B");
        InputNode iNode3 = (InputNode) PrototypeFactory.getInstance(PrototypeFactory.ModelType.INPUT);
        iNode3.setValue(false);
        iNode3.setName("Cin");

        ProbeNode pNode1 = (ProbeNode) PrototypeFactory.getInstance(PrototypeFactory.ModelType.PROBE);
        pNode1.setName("Cout");
        ProbeNode pNode2 = (ProbeNode) PrototypeFactory.getInstance(PrototypeFactory.ModelType.PROBE);
        pNode2.setName("S");

        ORNode orNode1 = (ORNode) PrototypeFactory.getInstance(PrototypeFactory.ModelType.OR);
        //((CompositeNode) orNode1).setStrategy(new ORNodeStrategy());

        nodes.add(iNode1);
        nodes.add(iNode2);
        nodes.add(iNode3);
        nodes.add(pNode1);
        nodes.add(pNode2);
        nodes.add(orNode1);

        for (Node node : nodes) {
            //System.out.println(node.toString() + " " + node.value + " " + node.name);
        }

        /* PART II */
        Subject s = new Subject(); // Composite

        s.setValue(true);
        s.setName("A");

        Subject s2 = new Subject(); // Composite
        s2.setValue(true);

        s.addChild(s2);

        s.print();

    }

}
