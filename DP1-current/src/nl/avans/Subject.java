package nl.avans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sander on 30-4-2015.
 */
public class Subject implements Component {

    private List<Observer> observers = new ArrayList<Observer>();
    private List<Subject> _children;
    private List<Boolean> inputs;
    private boolean value;
    private String name;

    public void addChild(Subject s) {
        _children.add(s);
        s.attach(new ORNodeObserver()); // ConcreteObserver / ConcreteStrategy
    }

    public List<Subject> getChildren(){
        return _children;
    }

    public void attach(Observer observer){
        observers.add(observer);
    }

    public void notifyObservers(){
        for (Observer observer : observers ){
            observer.calculate();
        }
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
        notifyObservers();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void print() {
        for (Component c : _children) {
            c.print();
            System.out.println("printing Subject" + getName());
        }
    }
}
