package nl.avans;

import javafx.beans.Observable;

/**
 * Created by Sander on 30-4-2015.
 */
public interface INodeStrategy {
    public void operation();
}