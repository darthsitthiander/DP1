package nl.avans;

/**
 * Created by Sander on 29-4-2015.
 */
public class InputNode extends Node implements PrototypeCapable {
    public InputNode(String name) {
        super(name);
    }

    public InputNode() {
        super();
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public InputNode clone(){
        try {
            return (InputNode) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void perform() {
        System.out.println(this.name);
    }

    public String toString() {
        return "input";
    }
}