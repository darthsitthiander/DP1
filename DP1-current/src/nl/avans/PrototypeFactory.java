package nl.avans;

import java.util.HashMap;

/**
 * Created by Sander on 30-4-2015.
 */
public class PrototypeFactory {

    public static class ModelType {
        public static final String INPUT = "input";
        public static final String PROBE = "probe";
        public static final String OR = "or";
    }

    private static HashMap<String, PrototypeCapable> prototypes = new HashMap<String, PrototypeCapable>();

    static {
        prototypes.put(ModelType.INPUT, new InputNode());
        prototypes.put(ModelType.PROBE, new ProbeNode());
        prototypes.put(ModelType.OR, new ORNode("OR"));
    }

    public static PrototypeCapable getInstance(final String s){
        return ((PrototypeCapable) prototypes.get(s)).clone();
    }

}