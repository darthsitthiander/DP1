package nl.avans;

/**
 * Created by Sander on 29-4-2015.
 */
public class ConcreteDecoratorANDNode extends DecoratorNode {


    public ConcreteDecoratorANDNode(String name) {
        super(name);
    }

    @Override
    public void setValue(boolean value) {

    }

    @Override
    public void perform() {
        addedBehavior();
        super.perform();
    }

    private void addedBehavior() {
        System.out.println("addedB");
    }
}
