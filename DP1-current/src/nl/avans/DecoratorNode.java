package nl.avans;

/**
 * Created by Sander on 29-4-2015.
 */
public abstract class DecoratorNode extends Node {

    protected Node node;

    public DecoratorNode(String name) {
        super(name);
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Override
    public void perform(){
        if (node != null) {
            node.perform();
        }
    }
}
