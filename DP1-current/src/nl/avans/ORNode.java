package nl.avans;

/**
 * Created by Sander on 30-4-2015.
 */
public class ORNode extends Node implements PrototypeCapable {
    public ORNode(String name) {
        super(name);
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public ORNode clone(){
        try {
            return (ORNode) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void perform() {
        System.out.println(this.name);
    }

    public String toString() {
        return "OR-Node";
    }
}