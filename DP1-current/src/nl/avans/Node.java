package nl.avans;

/**
 * Created by Sander on 29-4-2015.
 */
public abstract class Node {
    protected String name;
    protected boolean value;

    public Node(String name)
    {
        this.name = name;
    }

    public Node() {

    }

    public abstract void setValue(boolean value);

    public abstract void perform();

    public void setName(String name) {
        this.name = name;
    }
}