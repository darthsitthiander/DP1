package nl.avans;

import nl.avans.nodes.Node;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

public class PrototypeFactory
{
    private static PrototypeFactory service;
    private ServiceLoader<Node> loader;
    private HashMap<String, Node> nodes;

    private PrototypeFactory()
    {
        nodes                       = new HashMap<>();
        loader                      = ServiceLoader.load(Node.class);

        for (Node node : loader)
        {
            nodes.put(node.getClass().getSimpleName(), node);
        }
    }

    public static synchronized PrototypeFactory getInstance()
    {
        if (service == null)
        {
            service                 = new PrototypeFactory();
        }

        return service;
    }

    public Node getNodeInstance (final String s)
    {
        Node node                   = null;

        try
        {
            Iterator<Node> nodes    = loader.iterator();
            while (node == null && nodes.hasNext())
            {
                Node n  = nodes.next();

                if (n.getClass().getSimpleName().equalsIgnoreCase(s))
                {
                    node            =  n.clone();
                }
            }
        } catch (ServiceConfigurationError serviceError)
        {
            node                    = null;
            serviceError.printStackTrace();

        }

        return node;
    }
}
