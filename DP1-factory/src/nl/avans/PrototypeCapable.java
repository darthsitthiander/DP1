package nl.avans;

public interface PrototypeCapable extends Cloneable {
    public PrototypeCapable clone();
}