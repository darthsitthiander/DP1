package nl.avans.nodes;

public class INPUT_LOW extends Node
{
    public INPUT_LOW()
    {
        setValue(false);
    }

    @Override
    public void perform() {
        System.out.println(this.name);
    }
}