package nl.avans.nodes;

import nl.avans.PrototypeCapable;

public abstract class Node implements PrototypeCapable
{
    protected String name;
    protected boolean value;

    public Node clone(){
        try {
            return (Node) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getName ()
    {
        return name;
    }

    public void setValue (boolean value)
    {
        this.value = value;
    }

    public boolean getValue ()
    {
        return value;
    }

    public abstract void perform();
}