package nl.avans;

import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import nl.avans.nodes.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        ArrayList<Node> nodes = new ArrayList<Node>();

        //text representative of the full-header file of BB: https://bb.avans.nl/bbcswebdav/pid-5615225-dt-content-rid-12534766_2/courses/AII-1415D-INDP1/circuit1.txt

        Node iNode1 = PrototypeFactory.getInstance().getInstance("INPUT_HIGH");
        iNode1.setName("A");

        Node iNode2 = PrototypeFactory.getInstance().getInstance("INPUT_LOW");
        iNode2.setName("B");

        Node iNode3 = PrototypeFactory.getInstance().getInstance("INPUT_LOW");
        iNode3.setName("Cin");

        Node pNode1 = PrototypeFactory.getInstance().getInstance("PROBE");
        pNode1.setName("Cout");

        Node pNode2 = PrototypeFactory.getInstance().getInstance("PROBE");
        pNode2.setName("S");


        Node node1 = PrototypeFactory.getInstance().getInstance("OR");
        node1.setName("NODE1");

        Node node2 = PrototypeFactory.getInstance().getInstance("AND");
        node2.setName("NODE2");

        Node node3 = PrototypeFactory.getInstance().getInstance("AND");
        node3.setName("NODE3");

        Node node4 = PrototypeFactory.getInstance().getInstance("NOT");
        node4.setName("NODE4");

        Node node5 = PrototypeFactory.getInstance().getInstance("AND");
        node5.setName("NODE5");

        Node node6 = PrototypeFactory.getInstance().getInstance("OR");
        node6.setName("NODE6");

        Node node7 = PrototypeFactory.getInstance().getInstance("NOT");
        node7.setName("NODE7");

        Node node8 = PrototypeFactory.getInstance().getInstance("NOT");
        node8.setName("NODE8");

        Node node9 = PrototypeFactory.getInstance().getInstance("AND");
        node9.setName("NODE9");

        Node node10 = PrototypeFactory.getInstance().getInstance("AND");
        node10.setName("NODE10");

        Node node11 = PrototypeFactory.getInstance().getInstance("OR");
        node11.setName("NODE11");

        nodes.add(iNode1);
        nodes.add(iNode2);
        nodes.add(iNode3);
        nodes.add(pNode1);
        nodes.add(pNode2);

        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        nodes.add(node4);
        nodes.add(node5);
        nodes.add(node6);
        nodes.add(node7);
        nodes.add(node8);
        nodes.add(node9);
        nodes.add(node10);
        nodes.add(node11);

        for (Node node : nodes) {
            System.out.println(node.toString() + " " + node.getValue() + " " + node.getName());
        }

*/
        //System.out.println("Enter the name or route to a file to use as input : ");

        String fileName = "circuit.txt";

        /*
        Scanner scanIn = new Scanner(System.in);
        fileName = scanIn.nextLine();

        scanIn.close();
*/
        System.out.println("Going to read file: " + fileName);

        ArrayList<String> nodesText     = new ArrayList<>();
        ArrayList<String> edgesText     = new ArrayList<>();
        boolean readingEdges            = false;

        try
        {
            FileReader fr = new FileReader(fileName);

            BufferedReader br   = new BufferedReader(fr);
            String s;

            while ((s = br.readLine()) != null)
            {
                s = s.replaceAll("\\t", "").replaceAll("\\n", "").replaceAll(" ", "").replaceAll(";", "");

                if (s.length() == 0)
                {
                  readingEdges = true;
                    continue;
                }
                else
                {
                    if (s.charAt(0) != '#')
                    {
                        if (readingEdges)   { edgesText.add(s); }
                        else                { nodesText.add(s); }
                    }
                }
            }

            fr.close();
        }
        catch (FileNotFoundException e)     { System.out.println("File: " + fileName + " does not exist. Exiting program."); System.exit(0); }
        catch (IOException e)               { System.out.println("Failed to read file: " + fileName + ". Exiting program."); System.exit(0); }

        //System.out.println("\n nodes: \n");

        for (String s : nodesText) {
            //System.out.println(s);
        }

        //System.out.println("\n edges: \n");

        for (String s : edgesText) {
           // System.out.println(s);
        }

        System.out.println("done reading");

        ArrayList<Node> nodes = new ArrayList<>();

        String[] nodeData;
        String nodeName, nodeValue;

        for (String s : nodesText)
        {
            nodeData = s.split(":");
            nodeName = nodeData[0];
            nodeValue = nodeData[1];

            Node node = PrototypeFactory.getInstance().getNodeInstance(nodeValue);
            node.setName(nodeName);

            nodes.add(node);
        }

        for (Node node : nodes) {
            System.out.println(node.toString() + " " + node.getValue() + " " + node.getName());
        }
    }
}
