package nl.avans;

import java.util.HashMap;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

public class NodeFactory
{
    private static NodeFactory instance;
    private ServiceLoader<NodeStrategy> loader;
    private HashMap<String, NodeStrategy> nodeStrategies;

    private NodeFactory()
    {
        nodeStrategies                       = new HashMap<>();
        loader                               = ServiceLoader.load(NodeStrategy.class);

        for (NodeStrategy nodeStrategy : loader)
        {
            nodeStrategies.put(nodeStrategy.getClass().getSimpleName(), nodeStrategy);
        }
    }

    public static synchronized NodeFactory getInstance()
    {
        if (instance == null)
        {
            instance                 = new NodeFactory();
        }

        return instance;
    }

    public NodeStrategy getNodeStrategy (final String strategyName, Mediator mediator)
    {
        NodeStrategy nodeStrategy        = null;

        try
        {
            Iterator<NodeStrategy> nodeStrategies    = loader.iterator();

            while (nodeStrategy == null && nodeStrategies.hasNext())
            {
                NodeStrategy strategy  = nodeStrategies.next();

                if (strategy.getClass().getSimpleName().equalsIgnoreCase(strategyName))
                {
                    nodeStrategy            =  strategy.clone(mediator);
                }
            }
        } catch (ServiceConfigurationError serviceError)
        {
            nodeStrategy                    = null;
            System.out.println("ServiceConfigurationError: " + serviceError.getMessage() + ". Exiting program.");
            System.exit(1);
        }

        return nodeStrategy;
    }
}
