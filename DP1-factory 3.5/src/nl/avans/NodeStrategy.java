package nl.avans;

import java.util.List;
import java.util.Observable;

public abstract class NodeStrategy extends Observable implements Cloneable
{
    public abstract boolean perform (List<Boolean> inputs, String nodeName);

    public NodeStrategy clone (Mediator mediator)
    {
        NodeStrategy clone                            = null;

        try
        {
            clone                                   = (NodeStrategy) super.clone();
            clone.registerMediator(mediator);
        }
        catch (CloneNotSupportedException e)        { e.printStackTrace(); }

        return clone;
    }

    public void registerMediator (Mediator mediator)     {}
}
