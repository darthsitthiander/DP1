package nl.avans.strategies;

import nl.avans.NodeStrategy;

import java.util.List;

public class NOT extends NodeStrategy
{
    @Override
    public boolean perform(List<Boolean> inputs, String nodeName) {
        return !inputs.get(0);
    }
}