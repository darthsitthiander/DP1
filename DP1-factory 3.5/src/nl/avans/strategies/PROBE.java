package nl.avans.strategies;

import nl.avans.Mediator;
import nl.avans.NodeStrategy;

import java.util.List;
import java.util.Observable;

public class PROBE extends NodeStrategy
{
    @Override
    public void registerMediator (Mediator mediator)
    {
        mediator.registerPROBEStrategy(this);
    }

    @Override
    public boolean perform(List<Boolean> inputs, String nodeName) {
        System.out.println("Probe '" + nodeName + "' = " + (inputs.get(0) ? 1 : 0));

        this.setChanged();
        this.notifyObservers(inputs.get(0));

        return inputs.get(0);
    }
}