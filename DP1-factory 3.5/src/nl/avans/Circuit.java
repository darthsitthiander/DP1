package nl.avans;

import nl.avans.helpers.NodeBinder;
import nl.avans.helpers.NodePerformer;
import nl.avans.strategies.PROBE;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Circuit implements Mediator, Observer
{
    private ArrayList<INPUT> inputStrategies;
    private ArrayList<PROBE> probeStrategies;
    private ArrayList<Node> nodes;
    private ArrayList<Boolean> outputs;

    private NodeBinder binder;

    public Circuit (String circuitFileName)
    {
        this.inputStrategies        = new ArrayList<>();
        this.probeStrategies        = new ArrayList<>();
        this.outputs                = new ArrayList<>();

        this.binder                = new NodeBinder(circuitFileName, this);
        this.nodes                 = this.binder.getNodes();
    }

    public void setInput (int inputIndex, boolean inputValue)
    {
        try                                         { this.inputStrategies.get(inputIndex).setInputValue(inputValue); }
        catch (IndexOutOfBoundsException exception) { System.out.println("Got IndexOutOfBoundsException when setting input: " + exception.getMessage() + ". Exiting program."); System.exit(1); }
    }

    public ArrayList<Boolean> getOutputs ()
    {
        NodePerformer.perform(this.nodes);

        if (this.outputs.size() != this.probeStrategies.size())
        {
            System.out.println("Error in circuit! Not all the probe's have recieved their output. Exiting program.");
            System.exit(1);
        }

        return this.outputs;
    }

    @Override
    public void registerINPUTStrategy(INPUT inputStrategy)
    {
        this.inputStrategies.add(inputStrategy);
    }

    @Override
    public void registerPROBEStrategy(PROBE probeStrategy)
    {
        this.probeStrategies.add(probeStrategy);
        probeStrategy.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        boolean result = (boolean) arg;

        this.outputs.add(result);
    }
}
