package nl.avans.helpers;

import nl.avans.Node;
import java.util.ArrayList;

public class NodePerformer
{
    public static void perform (ArrayList<Node> nodes)
    {
        nodes.stream().filter(node -> node.hasZeroDependencies()).forEach(nl.avans.Node::perform);
    }
}
