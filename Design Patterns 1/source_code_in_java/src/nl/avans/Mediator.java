package nl.avans;

import nl.avans.strategies.PROBE;

public interface Mediator
{
    void registerINPUTStrategy(INPUT inputStrategy);
    void registerPROBEStrategy(PROBE probeStrategy);
}
