package nl.avans;

import java.util.ArrayList;

public class Main
{
    public static void main (String[] args)
    {
        System.out.println("Logica simulator versie 0.14 build 1.");

        System.out.println("Output for all probe nodes: \n");

        System.out.println("Circuit 1:");


        Circuit circuit1 = new Circuit("circuit1.txt");

        ArrayList<Boolean> outputs_circuit1 = circuit1.getOutputs();


        System.out.println("\nCircuit 3:");

        Circuit circuit2 = new Circuit("circuit3.txt");

        circuit2.setInput(0, outputs_circuit1.get(1));

        circuit2.getOutputs();

        System.out.println("...bye");
    }
}
