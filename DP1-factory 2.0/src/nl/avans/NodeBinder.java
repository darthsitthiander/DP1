package nl.avans;

import nl.avans.nodes.Node;

import java.util.ArrayList;

public class NodeBinder
{
    private ArrayList<String> nodesLines;
    private ArrayList<String> edgesLines;
    private ArrayList<Node> nodes;

    public NodeBinder (String fileName)
    {
        this.nodesLines                     = new ArrayList<>();
        this.edgesLines                     = new ArrayList<>();
        this.nodes                          = new ArrayList<>();

        FileReader fileReader               = new FileReader(fileName);

        this.separateLines(fileReader.getReadedLines());

        this.bindNodes();
    }

    private void separateLines (ArrayList<String> readedLines)
    {
        boolean readingEdges                = false;

        for (String s : readedLines)
        {
            if (s.length() == 0)
            {
                readingEdges                = true;
                continue;
            }
            else
            {
                if (s.charAt(0) != '#')
                {
                    if (readingEdges)       { this.edgesLines.add(s); }
                    else                    { this.nodesLines.add(s); }
                }
            }
        }
    }

    private void bindNodes ()
    {
        String[] nodeData, dependencyNodes;
        String nodeName, nodeValue;

        for (String s : this.nodesLines)
        {
            nodeData                        = s.split(";")[0].split(":");
            nodeName                        = nodeData[0];
            nodeValue                       = nodeData[1];

            Node node                       = PrototypeFactory.getInstance().getNodeInstance(nodeValue);
            node.setName(nodeName);

            this.nodes.add(node);
        }

        for (String s : this.edgesLines)
        {
            nodeData                        = s.split(";")[0].split(":");
            nodeName                        = nodeData[0];
            dependencyNodes                 = nodeData[1].split(",");

            Node currentNode                = null;

            for (Node node : this.nodes)
            {
                if (node.getName().equalsIgnoreCase(nodeName))
                {
                    currentNode             = node;
                    break;
                }
            }

            if (currentNode == null)        { System.out.println("An invalid name for a node was entered in the file. Exiting program."); System.exit(0); }

            for (String d : dependencyNodes)
            {
                for (Node node : this.nodes)
                {
                    if (node.getName().equalsIgnoreCase(d))
                    {
                        node.addDependency(currentNode);
                        break;
                    }
                }
            }
        }
    }

    public ArrayList<Node> getNodes ()      { return this.nodes; }
}