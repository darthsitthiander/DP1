package nl.avans;

public class Main
{
    public static void main (String[] args)
    {
        System.out.println("Logica simulator versie 0.14 build 1.");

        String fileName = "";//                                 = new InputReceiver().receiveInput();

        fileName                                        = (fileName.length() == 0) ? "circuit.txt" : fileName;

        NodeBinder binder               = new NodeBinder(fileName);

        NodePerformer.perform(binder.getNodes());
    }
}
