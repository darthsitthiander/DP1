package nl.avans;

public interface PrototypeCapable extends Cloneable {
    PrototypeCapable clone();
}