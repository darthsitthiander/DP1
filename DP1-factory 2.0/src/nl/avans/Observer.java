package nl.avans;

import nl.avans.nodes.Node;

public interface Observer
{
    void update (Node observable, Object args);
}
