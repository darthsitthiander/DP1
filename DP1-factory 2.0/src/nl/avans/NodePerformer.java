package nl.avans;

import nl.avans.nodes.Node;
import java.util.ArrayList;

public class NodePerformer
{
    public static void perform (ArrayList<Node> nodes)
    {
        System.out.println("Output for all probe nodes: \n");

        nodes.forEach(nl.avans.nodes.Node::perform);

        System.out.println("...bye");
    }
}
