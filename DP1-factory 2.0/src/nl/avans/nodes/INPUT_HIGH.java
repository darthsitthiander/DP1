package nl.avans.nodes;

public class INPUT_HIGH extends Node
{
    public INPUT_HIGH()     { setValue(true);  }

    @Override
    public void perform()
    {

        System.out.println(this.getName() + " performed");

        if (!broadCasted)
        {
            broadCasted     = false;

            setChanged();
            notifyObservers(this.getValue());
        }

    }
}