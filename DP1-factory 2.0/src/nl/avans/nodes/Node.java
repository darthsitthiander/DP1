package nl.avans.nodes;

import nl.avans.PrototypeCapable;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public abstract class Node extends Observable implements PrototypeCapable, Observer, Cloneable
{
    protected List<Node> dependencies;
    protected List<Boolean> inputs;

    protected String name;
    protected boolean value;
    protected boolean broadCasted                   = false;

    public Node clone ()
    {
        Node clone                                  = null;

        try
        {
            clone                                   = (Node) super.clone();
            clone.dependencies                      = new ArrayList<>();
            clone.inputs                            = new ArrayList<>();
        }
        catch (CloneNotSupportedException e)        { e.printStackTrace(); }

        return clone;
    }

    public void setName (String name)               { this.name = name; }

    public String getName ()                        { return name; }

    public void setValue (boolean value)            { this.value = value; }

    public boolean getValue ()                      { return value; }

    public void perform ()                          { }//dependencies.forEach(nl.avans.nodes.Node::perform); }

    public void addDependency (Node node)
    {
        dependencies.add(node);
        node.addObserver(this);
    }

    public void update (Observable obs, Object x)   {}
}