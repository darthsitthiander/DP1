package nl.avans.nodes;

import java.util.Observable;

public class PROBE extends Node
{
    @Override
    public void update (Observable obs, Object x)
    {
        boolean result          = (boolean) x;

        Node observableNode     = (Node) obs;

        this.dependencies.stream().filter(n -> observableNode.name.equalsIgnoreCase(n.name)).forEach(n -> {
            System.out.println("Probe '" + this.name + "' = " + (result ? 1 : 0));
        });
    }
}