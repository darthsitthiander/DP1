package nl.avans.nodes;

import java.util.Observable;

public class NOT extends Node
{
    @Override
    public void update (Observable obs, Object x)
    {
        boolean result              = (boolean) x;

        Node observableNode         = (Node) obs;

        this.dependencies.stream().filter(n -> observableNode.name.equalsIgnoreCase(n.name)).forEach(n -> {

            this.inputs.add(result);

            if (this.inputs.size() == 1)
            {
                System.out.println(this.getName() + " recieved " + result + " from " + observableNode.getName());

                this.value          = !this.inputs.get(0);

                setChanged();
                notifyObservers(this.value);
            }
        });
    }
}