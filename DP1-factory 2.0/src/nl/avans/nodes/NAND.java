package nl.avans.nodes;

import java.util.Observable;

public class NAND extends Node
{
    @Override
    public void update (Observable obs, Object x)
    {
        boolean result              = (boolean) x;

        Node observableNode         = (Node) obs;

        this.dependencies.stream().filter(n -> observableNode.name.equalsIgnoreCase(n.name)).forEach(n -> {

            this.inputs.add(result);

            if (this.inputs.size() == 2)
            {
                System.out.println(this.getName() + " recieved " + result + " from " + observableNode.getName());

                this.value          = !(this.inputs.get(0) && this.inputs.get(1));

                setChanged();
                notifyObservers(this.value);
            }
        });
    }
}