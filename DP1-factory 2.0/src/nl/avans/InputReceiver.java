package nl.avans;

import java.util.Scanner;

public class InputReceiver
{
    public String receiveInput ()
    {
        System.out.println("Enter the name or route to a file to use as input:");

        Scanner scanIn      = new Scanner(System.in);

        String input        = scanIn.nextLine();

        scanIn.close();

        return input;
    }
}
